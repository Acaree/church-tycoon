﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ActiveHelpUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public GameObject go;

    public void OnPointerEnter(PointerEventData eventData)
    {
        go.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        go.SetActive(false);
    }
}
