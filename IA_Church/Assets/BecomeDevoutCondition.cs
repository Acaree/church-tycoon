using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;
using UnityEngine.AI;


namespace NodeCanvas.Tasks.Conditions
{

    public class BecomeDevoutCondition : ConditionTask
    {

        public BBParameter<int> become_devout_probability;

       
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit()
        {
            return null;
        }

        //Called once per frame while the condition is active.
        //Return whether the condition is success or failure.
        protected override bool OnCheck()
        {
            bool ret = false;
            int rand = Random.Range(0, 100);

            float probability = become_devout_probability.value * (NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB").GetValue<float>("stand_bar_prob")/100);
            if (rand <= probability)
            {
                NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB").SetValue("new_conversion", true);
                NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB").SetValue("citizen_converted", agent.gameObject.transform.position);
                ret = true;
            }
            return ret;
        }
    }
}