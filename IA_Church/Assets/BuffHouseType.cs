﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class BuffHouseType : MonoBehaviour {

    public int house_type;


    bool avaliable = true;

    float become;
    float visit_stand;

    private void OnTriggerEnter(Collider other)
    {
        avaliable = false;

        GlobalBlackboard blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");

        if(house_type == 0)
        {
            become = blackboard.GetValue<float>("Home1_become_devout");
            visit_stand = blackboard.GetValue<float>("Home1_visit_stand");

            blackboard.SetValue("Home1_become_devout", become + 1.0f);
            blackboard.SetValue("Home1_visit_stand", visit_stand + 1.0f);
        }
        else if(house_type == 1)
        {
            become = blackboard.GetValue<float>("Home2_become_devout");
            visit_stand = blackboard.GetValue<float>("Home2_visit_stand");

            blackboard.SetValue("Home1_become_devout", become + 1.0f);
            blackboard.SetValue("Home1_visit_stand", visit_stand + 1.0f);
        }
        else
        {
            become = blackboard.GetValue<float>("Home3_become_devout");
            visit_stand = blackboard.GetValue<float>("Home3_visit_stand");


            blackboard.SetValue("Home3_become_devout",become + 1.0f);
            blackboard.SetValue("Home3_visit_stand", visit_stand + 1.0f);
        }

    }

    public void OnTriggerExit(Collider other)
    {
        avaliable = true;


        GlobalBlackboard blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");

        if (house_type == 0)
        {
            become = blackboard.GetValue<float>("Home1_become_devout");
            visit_stand = blackboard.GetValue<float>("Home1_visit_stand");

            blackboard.SetValue("Home1_become_devout", become - 1.0f);
            blackboard.SetValue("Home1_visit_stand", visit_stand - 1.0f);
        }
        else if (house_type == 1)
        {
            become = blackboard.GetValue<float>("Home2_become_devout");
            visit_stand = blackboard.GetValue<float>("Home2_visit_stand");

            blackboard.SetValue("Home1_become_devout", become - 1.0f);
            blackboard.SetValue("Home1_visit_stand", visit_stand - 1.0f);
        }
        else
        {
            become = blackboard.GetValue<float>("Home3_become_devout");
            visit_stand = blackboard.GetValue<float>("Home3_visit_stand");


            blackboard.SetValue("Home3_become_devout", become - 1.0f);
            blackboard.SetValue("Home3_visit_stand", visit_stand - 1.0f);
        }

    }




}
