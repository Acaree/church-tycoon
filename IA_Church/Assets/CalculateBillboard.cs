﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;


public class CalculateBillboard : MonoBehaviour {

    public int priest_taxes;
    public int votary_taxes;
    public int stands_taxes;
    public int church_taxes;

    int priest_number;
    int votary_number;
   

    //TODO get the global blackboard and assign votary_number, stands_number and priest_number


    public Text taxes_priest;
    public Text num_priest;

    public Text taxes_votary;
    public Text num_votary;

    public Text taxes_stands;
    public Text num_stands;

    public Text taxes_church;

    public Text total_taxes;
    public Text sum_total;

    public GameObject lose_btn;
    public GameObject continue_btn;

    
    // Use this for initialization
    void Start () {
       



	}


    private void Update()
    {
        GlobalBlackboard blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
        votary_number = blackboard.GetValue<int>("devouts_number");
        priest_number = blackboard.GetValue<int>("priest_number");
        int number_stands = blackboard.GetValue<int>("stands_active");
        int taxes = priest_taxes * priest_number + votary_taxes * votary_number + stands_taxes * number_stands + church_taxes;
        blackboard.SetValue("taxes", taxes);

    }

    public void Taxes()
    {
        GlobalBlackboard blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
        votary_number = blackboard.GetValue<int>("devouts_number");
        priest_number = blackboard.GetValue<int>("priest_number");
        int number_stands = blackboard.GetValue<int>("stands_active");
        int taxes = priest_taxes * priest_number + votary_taxes * votary_number + stands_taxes* number_stands + church_taxes;
        blackboard.SetValue("taxes", taxes);
    }

    public void CalculateAllBillboard()
    {
        if (gameObject.activeInHierarchy)
        {
            GlobalBlackboard blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
            votary_number = blackboard.GetValue<int>("devouts_number");

            taxes_priest.text = priest_taxes.ToString();
            taxes_votary.text = votary_taxes.ToString();
            taxes_stands.text = stands_taxes.ToString();
            taxes_church.text = church_taxes.ToString();

            votary_number = blackboard.GetValue<int>("devouts_number");
            priest_number = blackboard.GetValue<int>("priest_number");
            num_priest.text = priest_number.ToString();
            num_votary.text = votary_number.ToString();

            int number_stands = blackboard.GetValue<int>("stands_active");
            int taxes = priest_taxes * priest_number + votary_taxes * votary_number + stands_taxes * number_stands + church_taxes;

            total_taxes.text = taxes.ToString();

            int total = blackboard.GetValue<int>("money") - taxes;
            sum_total.text = total.ToString();

            blackboard.SetValue("money", total);
            blackboard.SetValue("taxes", taxes);
            if (total <= 0)
            {
                continue_btn.SetActive(false);
                lose_btn.SetActive(true);
                sum_total.text += " You lose";
                sum_total.color = Color.red;
                blackboard.SetValue("day_count", 0);
            }
            else
            {
                continue_btn.SetActive(true);
                lose_btn.SetActive(false);
                sum_total.text += " Day passed";
                sum_total.color = Color.green;
                blackboard.SetValue("day_count", blackboard.GetValue<int>("day_count") + 1);
            }


        }

    }



}
