﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;
public class CalculateMassMoney : MonoBehaviour {

    GlobalBlackboard blackboard;
    public Image Image;

    // Use this for initialization
    bool last_do_mass = false;
    private void Start()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
    }

    // Update is called once per frame
    void Update () {
        bool mass = blackboard.GetValue<bool>("do_masses");

        if(mass == false && last_do_mass == true)
        {
            Image.gameObject.GetComponent<AudioSource>().Play();
        }

        last_do_mass = mass;

	}


}
