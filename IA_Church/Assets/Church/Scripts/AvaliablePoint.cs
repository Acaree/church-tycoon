﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvaliablePoint : MonoBehaviour {

    public bool avaliable = true;

    float time = 0.0f;

    private void Update()
    {
       if(GameObject.Find("Directional Light").tag == "Night")
        {
            avaliable = true;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        avaliable = false;
        time = 0.0f;
    }

    public void OnTriggerExit(Collider other)
    {
        avaliable = true; 
    }

 
}
