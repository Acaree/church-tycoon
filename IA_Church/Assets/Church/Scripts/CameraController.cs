﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public float velocity = 20f;
    public float screen_border = 10f;

    public Vector2 top_positive_limit = new Vector2(80, 85);
    public Vector2 down_negative_limit = new Vector2(80, 5);

    // Update is called once per frame

    void Update()
    {

        Vector3 pos = transform.position;

        if (Input.GetKey("w") || Input.mousePosition.y >= Screen.height - screen_border)
        {
            pos.z += velocity * Time.deltaTime;
        }


        if (Input.GetKey("s") || Input.mousePosition.y <= screen_border)
        {
           
            
            pos.z -= velocity * Time.deltaTime;
        }


        if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - screen_border)
        {
          
            pos.x += velocity * Time.deltaTime;
        }


        if (Input.GetKey("a") || Input.mousePosition.x <= screen_border)
        {
            pos.x -= velocity * Time.deltaTime;
        }


        pos.x = Mathf.Clamp(pos.x, -top_positive_limit.x, down_negative_limit.x);
        pos.z = Mathf.Clamp(pos.z, -top_positive_limit.y, down_negative_limit.y);

        transform.position = pos;

    }
}
