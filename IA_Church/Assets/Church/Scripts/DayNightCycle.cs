﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class DayNightCycle : MonoBehaviour {

    public Light light;
    public float total_time_day = 20.0f;
    GlobalBlackboard blackboard;

    [Range(0, 1)]
    public float current_time = 0;

    float intensity_light;

    public GameObject go;

    // Use this for initialization
    void Start() {
        intensity_light = light.intensity;
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
    }

    // Update is called once per frame
    void Update() {

        if (tag == "Day")
            go.SetActive(false);
        else if(current_time>=0.99 || current_time <=0.01)
            go.SetActive(true);


        if (Time.timeScale != 0)
        {
            UpdateLight();

            current_time += (Time.deltaTime / total_time_day);

            if (current_time >= 1)
            {
                current_time = 0;
                GetComponent<PausePlayState>().PauseGame();
            }
               
        }

       
    }


    void UpdateLight()
    {
        float intensity_multiplier = 1;

        if (current_time <= 0.06f || current_time >= 0.75f)
        {
            intensity_multiplier = 0;
 
            tag = "Night";
        }
        else if (current_time <= 0.09f)
        {
            intensity_multiplier = Mathf.Clamp01((current_time - 0.06f) * (1 / 0.02f));
            tag = "Day";
        }
        else if (current_time >= 0.73f)
        {
            intensity_multiplier = Mathf.Clamp01(1 - ((current_time - 0.73f) * (1 / 0.02f)));
            tag = "Night";
        }
        else
        {
            tag = "Day";
        }
        light.intensity = (intensity_light * intensity_multiplier);
    }
}
