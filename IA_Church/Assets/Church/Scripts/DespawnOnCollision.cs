﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DespawnOnCollision : MonoBehaviour
{
    void OnCollisionEnter(Collision col)
    {
         Destroy(col.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }

}

