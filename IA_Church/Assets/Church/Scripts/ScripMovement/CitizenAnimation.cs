﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
public class CitizenAnimation : MonoBehaviour {

    private Animator animator;

    int movement;

    // Use this for initialization
    void Start()
    {

        animator = GetComponent<Animator>();
        movement = Animator.StringToHash("Movement");

    }

    // Update is called once per frame
    void Update()
    {

        if (GetComponent<Blackboard>().GetValue<bool>("search_destination") && GetComponent<Move>().movement == Vector3.zero)
            animator.SetBool(movement, false);
        else
            animator.SetBool(movement, true);

    }
}
