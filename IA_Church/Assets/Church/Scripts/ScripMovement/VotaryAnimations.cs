﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using NodeCanvas.Framework;

public class VotaryAnimations : MonoBehaviour {

    
    private Animator animator;

    int movement;
 
	// Use this for initialization
	void Start () {
        
        animator = GetComponent<Animator>();
        movement = Animator.StringToHash("Movement");
        
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 v = Vector3.zero;
        if (GetComponent<Blackboard>().GetValue<bool>("search_destination") && GetComponent<Move>().movement == Vector3.zero)
            animator.SetBool(movement, false);
        else
            animator.SetBool(movement, true);




    }
}
