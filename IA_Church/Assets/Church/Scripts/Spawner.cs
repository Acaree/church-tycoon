﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
public class Spawner : MonoBehaviour {

    //Controllers Respawn
    public float time_to_respawn = 0.5f;
    public float time_to_repeat = 0.5f;
    public bool its_night = false;
    public bool invoke_used = false;

    //Probability respawn
    public float respawn_probability_boy1 = 0.20f;
    public float respawn_probability_boy2 = 0.20f;
    public float respawn_probability_girl1 = 0.20f;
    public float respawn_probability_girl2 = 0.20f;
    public float respawn_probability_votary = 0.20f;

    //Position random respawn
    public float respawn_position_x = 0.50f;
    public float respawn_position_z = 0.50f;

    Light light;

    //GameObjects Respawn
    public int number_of_citizens;
    private int number_of_votary = 0;
    int respawned_citizens = 0;
    int respawned_votaries = 0;
    Vector3 position_spawn;
    public GameObject[] npc;

	void Start ()
    {
        position_spawn = transform.position;
        light = GameObject.Find("Directional Light").GetComponent<Light>();
    }
	
	// Update is called once per frame
	void Update () {

		if(light.tag == "Day" && !invoke_used)
        {
            InvokeRepeating("Respawn", time_to_respawn, time_to_repeat);
            invoke_used = true;
        }
        else if(light.tag == "Night" && invoke_used)
        {
            CancelInvoke();
            invoke_used = false;
            its_night = true;
            respawned_citizens = 0;
            respawned_votaries = 0;
        }
        

	}

    void Respawn()
    {

        number_of_votary = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB").GetValue<int>("devouts_number");

        number_of_citizens = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB").GetValue<int>("citizen_respwn");



        if (respawned_citizens < number_of_citizens)
        {
            Vector3 new_pos = new Vector3(transform.position.x + Random.Range(-respawn_position_x, respawn_position_x), 0, transform.position.z + Random.Range(-respawn_position_z, respawn_position_z));


            if (respawned_votaries < number_of_votary )
            {
                Instantiate(npc[4], new_pos, transform.rotation);
                respawned_votaries++;
            }
            else
            {


                float random_value = Random.value;
                int character_respawn = 0;


                if (random_value <= respawn_probability_boy1)
                {
                    character_respawn = 0;
                }
                else if (random_value <= respawn_probability_boy1 + respawn_probability_girl1)
                {
                    character_respawn = 1;
                }
                else if (random_value <= respawn_probability_boy1 + respawn_probability_girl1 + respawn_probability_boy2)
                {
                    character_respawn = 2;
                }
                else
                {
                    character_respawn = 3;
                }


                Instantiate(npc[character_respawn], new_pos, transform.rotation);
                respawned_citizens++;

            }
        }
    }
}
