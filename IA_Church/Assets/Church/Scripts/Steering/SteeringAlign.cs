﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringAlign : SteeringAbstract {

    public float min_angle = 0.01f;
    public float slow_angle = 0.1f;
    public float time_to_target = 0.01f;

    Move move;

    // Use this for initialization
    void Start()
    {
        move = GetComponent<Move>();
    }

    // Update is called once per frame
    void Update()
    {

        float current_angle = Mathf.Atan2(transform.forward.x, transform.forward.z) * Mathf.Rad2Deg;
        float target_angle = Mathf.Atan2(move.movement.x, move.movement.z) * Mathf.Rad2Deg;
        float angle_to_target = Mathf.DeltaAngle(current_angle, target_angle);
        float abs_angle_to_target = Mathf.Abs(angle_to_target);

        if (abs_angle_to_target >= min_angle)
        {

            if (abs_angle_to_target < slow_angle)
                angle_to_target = Mathf.LerpAngle(move.max_rot_velocity, abs_angle_to_target, slow_angle);
            else
                angle_to_target = move.max_rot_acceleration;

            angle_to_target /= time_to_target;

            move.AccelerateRotation(angle_to_target,priority);

        }
        else
        {
            move.SetRotationVelocity(0.0F); //Best way?
        }


    }
}
