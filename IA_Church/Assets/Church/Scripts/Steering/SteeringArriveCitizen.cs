﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SteeringArriveCitizen : SteeringAbstract
{

    Move move;
    Vector3[] corners_path;
    int path_count;

    public GameObject path_points;
   
    public bool search_objective = true;

    private NavMeshPath nav_mesh_path;
    private bool have_visited_monument = false;

    private float stop_time = 15.0f;

    public float min_distance = 1.0f;
    public float slow_distance = 5.0f;
    public float time_to_target = 0.1f;

    public float time_to_wait = 5.0f;


    Light light;

    bool despawn = true;
    int select_despawn = 0;

    public GameObject[] despawn_object;

    // Use this for initialization
    void Start()
    {
        move = GetComponent<Move>();
        nav_mesh_path = new NavMeshPath();
        SearchDestination();
        search_objective = false;

        light = GameObject.Find("Directional Light").GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {


        if (light.tag == "Night" && despawn)
        {
            Go_to_despawn();
        }
        else
        {
            if (!despawn && light.tag == "Night")
                InDespawnPositon();

            if (search_objective == false)
            {
                if (path_count < corners_path.Length)
                {
                    Steer(corners_path[path_count]);
                }
                else
                {
                    move.SetMovementVelocity(Vector3.zero);
                }
            }
            else
            {
                SearchDestination();
                search_objective = false;
            }
        }

    }

    public void Steer(Vector3 target)
    {
        if (!move)
            move = GetComponent<Move>();

        Vector3 ideal_vector = target - transform.position;
        float ideal_velocity = 0;

        if (ideal_vector.magnitude < min_distance)
        {
            if (path_count == corners_path.Length - 1)
            {
                move.SetMovementVelocity(Vector3.zero);
                search_objective = true;
            }

            path_count++;

        }
        else
        {

            if (path_count != (corners_path.Length - 1) || ideal_vector.magnitude > slow_distance)
            {
                ideal_velocity = move.max_mov_velocity;
            }
            else
            {
                ideal_velocity = move.max_mov_velocity * ideal_vector.magnitude / slow_distance;
            }

            Vector3 ideal_movement = ideal_vector.normalized * ideal_velocity;
            ideal_movement = (ideal_movement - move.movement) / time_to_target;
            ideal_movement.y = 0.0f;
            if (ideal_movement.magnitude > move.max_mov_acceleration)
            {
                ideal_movement.Normalize();
                ideal_movement *= move.max_mov_acceleration;
            }

            move.AccelerateMovement(ideal_movement, priority);

        }

    }


    void SearchDestination()
    {

        if (search_objective == true)
        {

            int points_count = path_points.transform.childCount;

            int random_point = 0;


            random_point = Random.Range(0, path_points.transform.childCount - 1);

            if (NavMesh.CalculatePath(transform.position, path_points.transform.GetChild(random_point).transform.position, NavMesh.GetAreaFromName("Wakable"), nav_mesh_path))
            {
                corners_path = nav_mesh_path.corners;
                path_count = 0;

            }


        }

    }


    void Go_to_despawn()
    {

        float random_value = Random.value;
        
        if (random_value <= 0.5f)
            select_despawn = 0;
        else
            select_despawn = 1;

        if (NavMesh.CalculatePath(transform.position, despawn_object[select_despawn].transform.position, NavMesh.GetAreaFromName("Wakable"), nav_mesh_path)) 
        {
            corners_path = nav_mesh_path.corners;
            path_count = 0;
            search_objective = false;
            despawn = false;
        }
    }

    void InDespawnPositon()
    {
        GameObject despawner = despawn_object[select_despawn];


        if (transform.position.x >= despawner.transform.position.x - despawner.transform.localScale.x / 2 && transform.position.x <= despawner.transform.position.x + despawner.transform.localScale.x / 2)
        {
            if (transform.position.z >= despawner.transform.position.z - despawner.transform.localScale.z / 2 && transform.position.z <= despawner.transform.position.z + despawner.transform.localScale.z / 2)
            {
                Destroy(gameObject);
            }

        }

    }


}