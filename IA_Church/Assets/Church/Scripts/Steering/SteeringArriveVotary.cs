﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;
using UnityEngine.UI;

public class SteeringArriveVotary : SteeringAbstract
{
 
    Move move;
    Vector3[] corners_path;
    int path_count;

    public GameObject path_points;
    public GameObject avaliable_pews;
    public bool search_objective = true;

    private NavMeshPath nav_mesh_path;
  
    public float min_distance = 1.0f;
    public float slow_distance = 5.0f;
    public float time_to_target = 0.1f;

    public bool in_church = false;
    bool first_time = true;
    
    bool go_pew = false;
    bool need_pew = true;
    bool stay_in_pew = false;
    public int selected_pew = -1;

    public float time_to_wait = 5.0f;
    float wait_time = 0.0f;

    [Range(0, 1)]
    public float probability = 0.5f;

    Image image;

    Light light;

    public GameObject[] despawn_object;
    bool despawn = true;
    int select_despawn = 0;
    Animator animator;

    // Use this for initialization
    void Start()
    {
        move = GetComponent<Move>();
        nav_mesh_path = new NavMeshPath();
        SearchDestination();
        image = GetComponentInChildren<Image>();
        image.enabled = false;
        light = GameObject.Find("Directional Light").GetComponent<Light>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (light.tag == "Night" && despawn)
        {
            Go_to_despawn();
        }
        else
        {
            if (!despawn && light.tag == "Night")
                InDespawnPositon();

            if (search_objective == false)
            {
                if (path_count < corners_path.Length)
                {
                    Steer(corners_path[path_count]);
                }
                else
                {
                    move.SetMovementVelocity(Vector3.zero);
                }
            }
            else
            {

                if (need_pew && in_church)
                {
                    float random_value = Random.value;

                    if (random_value <= probability)
                    {
                        go_pew = true;

                        need_pew = false;
                    }
                }


                if (go_pew)
                {
                    SearchPew();
                }
                else if (!go_pew && !need_pew && !stay_in_pew)
                {
                    StayInPew();
                    image.enabled = true;
                    animator.SetBool("Movement", true);
                }
                else
                {
                    animator.SetBool("Movement", false);
                    SearchDestination();
                    image.enabled = false;
                }
            }
        }

    }

    public void Steer(Vector3 target)
    {
        if (!move)
            move = GetComponent<Move>();

        Vector3 ideal_vector = target - transform.position;
        float ideal_velocity = 0;

        if (ideal_vector.magnitude < min_distance)
        {
            if (path_count == corners_path.Length - 1)
            {
                move.SetMovementVelocity(Vector3.zero);
                search_objective = true;
            }

            path_count++;

        }
        else
        {
            
            if (path_count != (corners_path.Length - 1) || ideal_vector.magnitude > slow_distance)
            {
                ideal_velocity = move.max_mov_velocity;
            }
            else
            {
                ideal_velocity = move.max_mov_velocity * ideal_vector.magnitude / slow_distance;
            }

            Vector3 ideal_movement = ideal_vector.normalized * ideal_velocity;
            ideal_movement = (ideal_movement - move.movement) / time_to_target;
            ideal_movement.y = 0.0f;
            if (ideal_movement.magnitude > move.max_mov_acceleration)
            {
                ideal_movement.Normalize();
                ideal_movement *= move.max_mov_acceleration;
            }

            move.AccelerateMovement(ideal_movement, priority);

        }

    }


    void SearchDestination()
    {
        
        if (search_objective == true)
        {
           
            int points_count = path_points.transform.childCount;

            int random_point = Random.Range(0, points_count - 1);

            if (first_time)
            {
                random_point = Random.Range(0, 2);
                first_time = false;
            }

            if (in_church)
                random_point = Random.Range(3, points_count - 1);

            if (random_point > 3)
                in_church = true;


            if (NavMesh.CalculatePath(transform.position, path_points.transform.GetChild(random_point).transform.position , NavMesh.GetAreaFromName("Wakable"), nav_mesh_path))
            {
                corners_path = nav_mesh_path.corners;
                path_count = 0;

                search_objective = false;
            }

            
        }

    }

    void SearchPew()
    {
        if (search_objective == true)
        {
          
          
            int points_count = avaliable_pews.transform.childCount;
            
            
            for(int i = 0; i < points_count; ++i)
            {
                selected_pew = Random.Range(0, points_count - 1);

                if(avaliable_pews.transform.GetChild(selected_pew).GetComponent<AvaliablePoint>().avaliable)
                {
                    break;
                }

                selected_pew = -1;

            }

            if(selected_pew != -1)
            {

                if (NavMesh.CalculatePath(transform.position, avaliable_pews.transform.GetChild(selected_pew).transform.position, NavMesh.GetAreaFromName("Wakable"), nav_mesh_path))
                {
                    corners_path = nav_mesh_path.corners;
                    path_count = 0;

                    search_objective = false;

                    
                    go_pew = false;
                }


            }



        }

    }

    void StayInPew()
    {
        if (wait_time >= time_to_wait)
            stay_in_pew = true;
        else
            wait_time += Time.deltaTime;
    }


    void Go_to_despawn()
    {

         float random_value = Random.value;

         if (random_value <= 0.5f)
             select_despawn = 0;
         else
             select_despawn = 1;
        

        if (NavMesh.CalculatePath(transform.position, despawn_object[select_despawn].transform.position, NavMesh.GetAreaFromName("Wakable"), nav_mesh_path)) 
        {
            corners_path = nav_mesh_path.corners;
            path_count = 0;
            search_objective = false;
            despawn = false;
        }
    }

    void InDespawnPositon()
    {
        GameObject despawner = despawn_object[select_despawn];


        if (transform.position.x >= despawner.transform.position.x - despawner.transform.localScale.x / 2 && transform.position.x <= despawner.transform.position.x + despawner.transform.localScale.x / 2)
        {
            if (transform.position.z >= despawner.transform.position.z - despawner.transform.localScale.z / 2 && transform.position.z <= despawner.transform.position.z + despawner.transform.localScale.z / 2)
            {
                Destroy(gameObject);
            }

        }

    }



}
