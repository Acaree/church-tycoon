﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class RayClass : System.Object
{
    public Vector3 direction = Vector3.zero;
    public float max_distance = 10.0F;
}

public class SteeringObstacleAvoidance : SteeringAbstract
{

    public LayerMask mask;
    public float avoid_distance = 5.0f;

    Move move;

    public RayClass[] rays;

    // Use this for initialization
    void Start()
    {
        move = GetComponent<Move>();
     
    }

    // Update is called once per frame
    void Update()
    {

        float angle = Mathf.Atan2(move.movement.x, move.movement.z);
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.up);

        foreach (RayClass ray in rays)
        {
            RaycastHit hit;

            if (Physics.Raycast(new Vector3(transform.position.x, 0.0f, transform.position.z), q * ray.direction.normalized, out hit, ray.max_distance, mask) == true)
            {
                Vector3 a = hit.normal.normalized * avoid_distance;
                move.AccelerateMovement(a,priority);
            }
        }

    }

    void OnDrawGizmosSelected()
    {
        if (move && this.isActiveAndEnabled)
        {
            Gizmos.color = Color.red;
            float angle = Mathf.Atan2(move.movement.x, move.movement.z);
            Quaternion q = Quaternion.AngleAxis(Mathf.Rad2Deg * angle, Vector3.up);


            foreach (RayClass ray in rays)
            {
              

                    Debug.DrawRay(move.transform.position, q * ray.direction.normalized * ray.max_distance, Color.green);
                
            }


        }
    }
}