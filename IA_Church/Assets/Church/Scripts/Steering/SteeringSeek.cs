﻿using UnityEngine;
using System.Collections;

public class SteeringSeek : SteeringAbstract
{
    Move move;

    // Use this for initialization
    void Start()
    {
        move = GetComponent<Move>();
    }

    // Update is called once per frame
    void Update()
    {
        Steer(move.movement); //need a target?¿
    }

    public void Steer(Vector3 target)
    {
        if (!move)
            move = GetComponent<Move>();

        Vector3 distance_to_target = target - transform.position;
        distance_to_target.Normalize();
        distance_to_target *= move.max_mov_acceleration;

        move.AccelerateMovement(distance_to_target, priority);
    }
}