﻿using UnityEngine;
using System.Collections;

public class SteeringSeparation : SteeringAbstract
{

    public LayerMask mask;
    public float search_radius;
    public AnimationCurve falloff;

    Move move;

    // Use this for initialization
    void Start()
    {
        move = GetComponent<Move>();
    }

    // Update is called once per frame
    void Update()
    {

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, search_radius, mask.value);
        int i = 0;
        Vector3 repulsion;
        repulsion = Vector3.zero;

        while (i < hitColliders.Length)
        {
            if (!(hitColliders[i].gameObject.transform.position == transform.position))
            {

                Vector3 escape_direction = transform.position - hitColliders[i].gameObject.transform.position;
                
                float force = (1.0F - falloff.Evaluate(escape_direction.magnitude/search_radius)) * move.max_mov_acceleration;
                
                if(force > move.max_mov_acceleration)
                {
                    force = move.max_mov_acceleration;
                }

               
                repulsion += escape_direction.normalized * force;

                repulsion.y = 0;
            }
            i++;
        }

       
       if(repulsion.magnitude > 0.0f)
        {
            if (repulsion.magnitude > move.max_mov_acceleration)
                repulsion = repulsion.normalized * move.max_mov_acceleration;

            if(move.movement != Vector3.zero)
             move.AccelerateMovement(repulsion, priority);
        }

    }

    void OnDrawGizmosSelected()
    {
       // Display the explosion radius when selected
       Gizmos.color = Color.yellow;
       Gizmos.DrawWireSphere(transform.position, search_radius);
    }

    void Triggered()
    {
        print("triggered");
    }
}