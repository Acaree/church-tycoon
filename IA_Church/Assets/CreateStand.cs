﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class CreateStand : MonoBehaviour {

    public GameObject[] go;
    public GameObject point_to_path;
    public GameObject stand_point_priest;
    public int num;
    public int cost_house1;
    public int cost_house2;
    public int cost_house3;
    public int taxes_house;

    GlobalBlackboard blackboard;

    public GameObject text_build;
    public GameObject text;
    void Start()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");

    }

    public void AssignTypeOfHouse(int x)
    {
        num = x;
        text_build.SetActive(true);
    }

    private void OnMouseDown()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
        int number = transform.parent.gameObject.GetComponent<CreateStand>().num;
        if (number != -1)
        {
            point_to_path.SetActive(true);
            point_to_path.tag = go[number].tag;
            Instantiate(go[number], transform.position, transform.rotation);
            this.gameObject.SetActive(false);
            transform.parent.gameObject.SetActive(false);

            if(number == 0)
            {   blackboard.SetValue("money", blackboard.GetValue<int>("money") - cost_house1);
                blackboard.SetValue("taxes", blackboard.GetValue<int>("taxes") + taxes_house);
            }
             
            else if(number == 1)
            {
                blackboard.SetValue("money", blackboard.GetValue<int>("money") - cost_house2);
                blackboard.SetValue("taxes", blackboard.GetValue<int>("taxes") + taxes_house);
            }
            else
            {
                blackboard.SetValue("money", blackboard.GetValue<int>("money") - cost_house3);
                blackboard.SetValue("taxes", blackboard.GetValue<int>("taxes") + taxes_house);
            }

            stand_point_priest.SetActive(true);

            blackboard.SetValue("stands_active", blackboard.GetValue<int>("stands_active") + 1);
            text_build.SetActive(false);
            text.SetActive(false);
        }
    }
}
