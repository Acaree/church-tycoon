﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class Decision_changes : MonoBehaviour {


    GlobalBlackboard blackboard;

    public int money_variation;
    public float Happines_variation;
    public float Citizens_to_Votary_variation;
    public int spawned_variation;
    public int money_for_masses;
    // Use this for initialization
    void Start () {

        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");

    }
	
	public void MoreChanceConvertCitizensLessMoney()
    {
        blackboard.SetValue("stand_bar_prob", blackboard.GetValue<float>("stand_bar_prob") + Citizens_to_Votary_variation);
        blackboard.SetValue("money", blackboard.GetValue<int>("money") - money_variation);

    }

    public void MoreMoneyLessVotaryHappines()
    {
        blackboard.SetValue("money", blackboard.GetValue<int>("money") + money_variation);
        blackboard.SetValue("votary_happines", blackboard.GetValue<float>("votary_happines") - Happines_variation);
    }

    public void MoreChanceConvertCitizensLessHappines()
    {
        blackboard.SetValue("stand_bar_prob", blackboard.GetValue<float>("stand_bar_prob") + Citizens_to_Votary_variation);
        blackboard.SetValue("votary_happines", blackboard.GetValue<float>("votary_happines") - Happines_variation);
    }

    public void MoreSpawnCitizenLessMoney()
    {
        blackboard.SetValue("citizen_respwn", blackboard.GetValue<int>("citizen_respwn") + spawned_variation);
        blackboard.SetValue("money", blackboard.GetValue<int>("money") - money_variation);
    }

    public void MoreSpawnCitizenLessChanceConvert()
    {
        blackboard.SetValue("citizen_respwn", blackboard.GetValue<int>("citizen_respwn") + spawned_variation);
        blackboard.SetValue("stand_bar_prob", blackboard.GetValue<float>("stand_bar_prob") - Citizens_to_Votary_variation);
    }

    public void MoreChanceConvertLessSpawn()
    {
        blackboard.SetValue("stand_bar_prob", blackboard.GetValue<float>("stand_bar_prob") + Citizens_to_Votary_variation);
        blackboard.SetValue("citizen_respwn", blackboard.GetValue<int>("citizen_respwn") - spawned_variation);
    }

    public void MoreSpawnCitizenLessVotaryHappy()
    {
        blackboard.SetValue("citizen_respwn", blackboard.GetValue<float>("citizen_respwn") + spawned_variation);
        blackboard.SetValue("votary_happines", blackboard.GetValue<float>("votary_happines") - Happines_variation);
    }

    public void MoreMoneyLessChanceConvert()
    {
        blackboard.SetValue("money", blackboard.GetValue<int>("money") + money_variation);
        blackboard.SetValue("stand_bar_prob", blackboard.GetValue<float>("stand_bar_prob") - Citizens_to_Votary_variation);
    }

    public void PriestConvertToVotary()
    {
        blackboard.SetValue("priest_number", blackboard.GetValue<int>("priest_number") - 1);
        blackboard.SetValue("devouts_number", blackboard.GetValue<int>("devouts_number") + 1);
    }

    public void MoreHappyLessChanceConvert()
    {
        blackboard.SetValue("votary_happines", blackboard.GetValue<float>("votary_happines") + Happines_variation);
        blackboard.SetValue("stand_bar_prob", blackboard.GetValue<float>("stand_bar_prob") - Citizens_to_Votary_variation);
    }

    public void MoreHappyLessMoney()
    {
        blackboard.SetValue("votary_happines", blackboard.GetValue<float>("votary_happines") + Happines_variation);
        blackboard.SetValue("money", blackboard.GetValue<int>("money") - money_variation);
    }

    public void MoreMoneyLessSpawn()
    {
        blackboard.SetValue("money", blackboard.GetValue<int>("money") + money_variation);
        blackboard.SetValue("citizen_respwn", blackboard.GetValue<int>("citizen_respwn") - spawned_variation);
    }

    public void MoreMoneyforMassesLessChanceConvert()
    {
        blackboard.SetValue("money_for_masses", blackboard.GetValue<int>("money_for_masses") + money_for_masses);
        blackboard.SetValue("stand_bar_prob", blackboard.GetValue<float>("stand_bar_prob") - Citizens_to_Votary_variation);
    }
}
