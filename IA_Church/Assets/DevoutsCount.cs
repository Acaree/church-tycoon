﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class DevoutsCount : MonoBehaviour {

	Text text;
	GlobalBlackboard blackboard;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		blackboard = NodeCanvas.Framework.GlobalBlackboard.Find ("Church_BB");
	}

	// Update is called once per frame
	void Update () {
        text.color = Color.black;
		int actual_devouts = blackboard.GetValue<int> ("devouts_number");
		int max_number = blackboard.GetValue<int> ("max_number_devouts");

		text.text = "Devouts: " + actual_devouts.ToString () + "/" + max_number.ToString ();

		if (actual_devouts == max_number)
		{
			text.color = Color.green;
            text.text = "Devouts: " + actual_devouts.ToString() + "/" + max_number.ToString();
		}
        else
            text.text = "Devouts: " + actual_devouts.ToString() + "/" + max_number.ToString();
    }
}
