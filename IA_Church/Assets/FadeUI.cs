﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class FadeUI : MonoBehaviour {

    public Image image;
    GlobalBlackboard blackboard;
    public GameObject particle_go;
    GameObject tmp_go;
    float time = 0;
	// Use this for initialization
	void Start () {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
	}
	
	// Update is called once per frame
	void Update () {
		
        if(blackboard.GetValue<bool>("new_conversion"))
        {
            if (!image.IsActive())
            {

                this.GetComponent<AudioSource>().Play();
                image.gameObject.SetActive(true);
                image.CrossFadeAlpha(0,3.0f,false);
                image.gameObject.transform.GetChild(0).GetComponent<Text>().CrossFadeAlpha(0, 3.0f, false);
                Instantiate(particle_go, blackboard.GetValue<Vector3>("citizen_converted"), Quaternion.identity);
                particle_go.GetComponent<ParticleSystem>().Play();
            }

            if (time >= 3.0)
            {
                time = 0;
                image.gameObject.SetActive(false);
                blackboard.SetValue("new_conversion", false);
                Destroy(tmp_go);

            }
            else
                time += Time.deltaTime;


        }

	}
}
