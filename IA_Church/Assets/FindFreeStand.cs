
using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;
using UnityEngine.AI;

namespace NodeCanvas.Tasks.Actions
{

    public class FindFreeStand : ActionTask
    {
        public BBParameter<GameObject> stands_parent;
        public BBParameter<GameObject> stand_going;
        public BBParameter<float> visit_stand_probability;
        public BBParameter<float> dont_visit_stand_probability;
        public BBParameter<float> become_devout_probability;

        public BBParameter<float> home1_become_devout_probability;
        public BBParameter<float> home1_visit_stand_probability;

        public BBParameter<float> home2_become_devout_probability;
        public BBParameter<float> home2_visit_stand_probability;

        public BBParameter<float> home3_become_devout_probability;
        public BBParameter<float> home3_visit_stand_probability;

        private int selected_stand_pos = -1;
        private int previous_stand_pos = -1;
        private bool stand_free = false;
        private int[] free_stands;

       
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit()
        {
            return null;
        }

        //This is called once each time the task is enabled.
        //Call EndAction() to mark the action as finished, either in success or failure.
        //EndAction can be called from anywhere.
        protected override void OnExecute()
        {
            stand_free = false;
            if (stands_parent.value.transform.childCount > 0)
            {
                free_stands = new int[stands_parent.value.transform.childCount];
                int j = 0;
                for (int i = 0; i < stands_parent.value.transform.childCount; i++)
                {
                    bool is_avaliable = stands_parent.value.transform.GetChild(i).gameObject.GetComponent<AvaliablePoint>().avaliable;
                    if (is_avaliable && stands_parent.value.transform.GetChild(i).gameObject.activeSelf) 
                    {
                        free_stands[j] = i;
                        j++;
                    }
                }

                if (j > 0)
                {
                    if (j > 1)
                    {
                        while (previous_stand_pos == selected_stand_pos)
                        {
                            selected_stand_pos = Random.Range(0, j);
                        }
                    }

                    else
                    {
                        selected_stand_pos = Random.Range(0, j);
                    }
                    previous_stand_pos = selected_stand_pos;
                    stand_free = true;
                    stand_going.value = stands_parent.value.transform.GetChild(free_stands[selected_stand_pos]).gameObject;

                    if (stand_going.value.tag == "House1")
                    {
                       
                        visit_stand_probability.value += home1_visit_stand_probability.value;
                      

                        dont_visit_stand_probability.value = 100 - visit_stand_probability.value;
                        become_devout_probability.value = home1_become_devout_probability.value;
                    }

                   else if (stand_going.value.tag == "House2")
                    {
                       
                        visit_stand_probability.value += home2_visit_stand_probability.value;
                       

                        dont_visit_stand_probability.value = 100 - visit_stand_probability.value;
                        become_devout_probability.value = home2_become_devout_probability.value;
                    }

                    else if (stand_going.value.tag == "House3")
                    {
                       
                        visit_stand_probability.value += home3_visit_stand_probability.value;
                      

                        dont_visit_stand_probability.value = 100 - visit_stand_probability.value;
                        become_devout_probability.value = home3_become_devout_probability.value;
                    }

                    stands_parent.value.transform.GetChild(free_stands[selected_stand_pos]).gameObject.GetComponent<AvaliablePoint>().avaliable = false; //carefull: this change the prefab, so next compilation will be changed
                }

            }

           EndAction(stand_free);


        }

        //Called once per frame while the action is active.
        protected override void OnUpdate()
        {

           
        }

        //Called when the task is disabled.
        protected override void OnStop()
        {


        }

        //Called when the task is paused.
        protected override void OnPause()
        {

        }
    }
}