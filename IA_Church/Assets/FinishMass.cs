using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace NodeCanvas.Tasks.Conditions{

	public class FinishMass : ConditionTask{

        GlobalBlackboard blackboard;

        protected override string OnInit()
        {

            blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
            return null;
        }

        //Called once per frame while the condition is active.
        //Return whether the condition is success or failure.
        protected override bool OnCheck(){

            if(blackboard.GetValue<bool>("do_masses"))
            {
                blackboard.SetValue("do_masses", false);
                blackboard.GetVariable<GameObject>("podium").value.GetComponent<AvaliablePoint>().avaliable = true;
                int i = blackboard.GetValue<int>("devouts_number");
                float j = blackboard.GetValue<float>("votary_happines");
                int money = blackboard.GetValue<int>("money_for_masses");
                float q = (float)i * j * money;
                blackboard.SetValue("money", blackboard.GetValue<int>("money") + (int)q);
            }


            return false;
        }
	}
}