using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace NodeCanvas.Tasks.Actions{

	public class GoDespawn : ActionTask{

        public BBParameter<GameObject> despawn_point;
        public BBParameter<bool> search_destination;
        SteeringArrive steer;
        NavMeshPath nav_mesh_path;

        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit(){

            steer = agent.GetComponent<SteeringArrive>();
            nav_mesh_path = new NavMeshPath();
            return null;
		}

		//This is called once each time the task is enabled.
		//Call EndAction() to mark the action as finished, either in success or failure.
		//EndAction can be called from anywhere.
		protected override void OnExecute(){

            Image image = agent.GetComponentInChildren<Image>();

            if(image != null)
             agent.GetComponentInChildren<Image>(true).enabled = false;
            if(agent.GetComponent<AudioSource>() != null)
              agent.GetComponent<AudioSource>().Stop();

            NavMesh.CalculatePath(agent.transform.position, despawn_point.value.transform.position, NavMesh.AllAreas, nav_mesh_path);
            steer.SetPathCorners(nav_mesh_path.corners);
            search_destination.value = false;
            //EndAction(false);
		}

		//Called once per frame while the action is active.
		protected override void OnUpdate(){
			if(GameObject.Find("Directional Light").GetComponent<DayNightCycle>().current_time > 0.05f && GameObject.Find("Directional Light").GetComponent<DayNightCycle>().current_time <= 0.07f)
            {
                GameObject.Destroy(agent.gameObject);
            }
		}

		//Called when the task is disabled.
		protected override void OnStop(){
			
		}

		//Called when the task is paused.
		protected override void OnPause(){
			
		}
	}
}