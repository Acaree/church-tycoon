using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;
using UnityEngine.AI;

namespace NodeCanvas.Tasks.Actions{

	public class GoToPoint : ActionTask{

        SteeringArrive steer;
        public BBParameter<GameObject> point;

        public BBParameter<bool> search_destination;

        Vector3[] final_path;
        NavMeshPath nav_mesh_path;
        AvaliablePoint avaliable;
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit()
        {

            steer = agent.GetComponent<SteeringArrive>();
            nav_mesh_path = new NavMeshPath();
            return null;
        }

        //This is called once each time the task is enabled.
        //Call EndAction() to mark the action as finished, either in success or failure.
        //EndAction can be called from anywhere.
        protected override void OnExecute()
        {
            avaliable = point.value.GetComponent<AvaliablePoint>();
            if (search_destination.value)
            {

                if (NavMesh.CalculatePath(agent.GetComponent<Transform>().position, point.value.transform.position, NavMesh.GetAreaFromName("Wakable"), nav_mesh_path))
                {
                    final_path = nav_mesh_path.corners;
                    steer.SetPathCorners(final_path);
                    search_destination.value = false;
                }
                else
                {
                    if (avaliable != null)
                        avaliable.avaliable = true;
                }

               
                //EndAction(true);
            }


        }

        //Called once per frame while the action is active.
        protected override void OnUpdate()
        {
             
            if (avaliable != null)
                avaliable.avaliable = false;
            if (search_destination.value || GameObject.Find("Directional Light").tag == "Night")
            {
                point.value = null;
                //TO TEST
               // point.value.GetComponent<AvaliablePoint>().avaliable = true;
                EndAction(true);
            }
        }

        //Called when the task is disabled.
        protected override void OnStop()
        {


        }

        //Called when the task is paused.
        protected override void OnPause()
        {

        }
    }
}