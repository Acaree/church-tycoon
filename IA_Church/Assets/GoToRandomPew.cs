using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;
using UnityEngine.AI;

namespace NodeCanvas.Tasks.Actions{

	public class GoToRandomPew : ActionTask{

        SteeringArrive steer;
        public BBParameter<GameObject> points_parent;

        int last_selection = -1;
        public BBParameter<bool> search_destination;

        Vector3[] final_path;
        NavMeshPath nav_mesh_path;
        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit()
        {

            steer = agent.GetComponent<SteeringArrive>();
            nav_mesh_path = new NavMeshPath();
            return null;
        }

        //This is called once each time the task is enabled.
        //Call EndAction() to mark the action as finished, either in success or failure.
        //EndAction can be called from anywhere.
        protected override void OnExecute()
        {

            if (search_destination.value)
            {
                int rand = Random.Range(0, points_parent.value.transform.childCount);

                while (last_selection == rand || points_parent.value.transform.GetChild(rand).GetComponent<AvaliablePoint>().avaliable == false)
                    rand = Random.Range(0, points_parent.value.transform.childCount);


                NavMesh.CalculatePath(agent.GetComponent<Transform>().position, points_parent.value.transform.GetChild(rand).position, NavMesh.GetAreaFromName("Wakable"), nav_mesh_path);
                final_path = nav_mesh_path.corners;
                steer.SetPathCorners(final_path);

                last_selection = rand;
                search_destination.value = false;

                points_parent.value.transform.GetChild(rand).GetComponent<AvaliablePoint>().avaliable = false;
                //EndAction(true);
            }


        }

        //Called once per frame while the action is active.
        protected override void OnUpdate()
        {

            if (search_destination.value)
            {
                points_parent.value.transform.GetChild(last_selection).GetComponent<AvaliablePoint>().avaliable = true;
                EndAction(true);
            }
        }

    }
}