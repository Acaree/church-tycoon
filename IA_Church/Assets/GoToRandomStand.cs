using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;
using UnityEngine.AI;

namespace NodeCanvas.Tasks.Actions
{

    public class GoToRandomStand : ActionTask
    {

        SteeringArrive steer;
        public BBParameter<GameObject> points_parent;

        int last_selection = -1;
        public BBParameter<bool> search_destination;

        Vector3[] final_path;
        NavMeshPath nav_mesh_path;
        GameObject[] tmp_go;

        //Use for initialization. This is called only once in the lifetime of the task.
        //Return null if init was successfull. Return an error string otherwise
        protected override string OnInit()
        {
            steer = agent.GetComponent<SteeringArrive>();
            nav_mesh_path = new NavMeshPath();

            return null;
        }

        //This is called once each time the task is enabled.
        //Call EndAction() to mark the action as finished, either in success or failure.
        //EndAction can be called from anywhere.
        protected override void OnExecute()
        {

            if (search_destination.value)
            {
                int size = 0;
                for (int i = 0; i < points_parent.value.transform.childCount; ++i)
                {
                    if (points_parent.value.transform.GetChild(i).gameObject.activeInHierarchy == true)
                        if (points_parent.value.transform.GetChild(i).GetComponent<AvaliablePoint>().avaliable == true)
                            size++;
                }

                tmp_go = new GameObject[size];

                size = 0;
                for (int i = 0; i < points_parent.value.transform.childCount; ++i)
                {
                    if (points_parent.value.transform.GetChild(i).gameObject.activeInHierarchy == true)
                        if (points_parent.value.transform.GetChild(i).GetComponent<AvaliablePoint>().avaliable == true)
                        {
                            tmp_go.SetValue(points_parent.value.transform.GetChild(i).gameObject, size);
                            size++;
                        }
                }

                int rand = Random.Range(0, tmp_go.Length - 1);



                NavMesh.CalculatePath(agent.GetComponent<Transform>().position, tmp_go[rand].transform.position, NavMesh.GetAreaFromName("Wakable"), nav_mesh_path);
                final_path = nav_mesh_path.corners;
                steer.SetPathCorners(final_path);

                last_selection = rand;
                search_destination.value = false;

                points_parent.value.transform.Find(tmp_go[rand].name).GetComponent<AvaliablePoint>().avaliable = false;


            }


        }

        //Called once per frame while the action is active.
        protected override void OnUpdate()
        {

            if (search_destination.value)
            {
                points_parent.value.transform.GetChild(last_selection).GetComponent<AvaliablePoint>().avaliable = true;
                EndAction(true);
            }
        }

    }
}
