﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class MoneyorTaxesVariation : MonoBehaviour {

    GlobalBlackboard blackboard;
    private int last_frame_money;
    private int last_frame_taxes;
    private bool fading_taxes = false;
    private bool fading_money = false;
    private float money_fade_time_count = 0;
    private float taxes_fade_time_count = 0;
    public float fade_time;
    public Text taxes_text;
    public Text money_text;

    bool first_time = true;
    // Use this for initialization
    void Start () {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
        last_frame_money = blackboard.GetValue<int>("money");
        last_frame_taxes = blackboard.GetValue<int>("taxes");
    }
	
	// Update is called once per frame
	void Update () {

        if (last_frame_money != blackboard.GetValue<int>("money"))
        {
            if (!first_time)
            {
                int money_balance = blackboard.GetValue<int>("money") - last_frame_money;

                if (money_balance > 0)
                {
                    money_text.text = "+";
                    money_text.text += money_balance.ToString();
                    money_text.color = new Color(0.2f, 0.7f, 0.2f, 1.0f);
                }

                else
                {
                    money_text.text = money_balance.ToString();
                    money_text.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
                }

                last_frame_money = blackboard.GetValue<int>("money");
                StartCoroutine(FadeTextToZeroAlpha(fade_time, money_text));
            }
        }

        if (last_frame_taxes != blackboard.GetValue<int>("taxes"))
        {
            if (!first_time)
            {
                int taxes_balance = blackboard.GetValue<int>("taxes") - last_frame_taxes;

                if (taxes_balance > 0)
                {
                    taxes_text.text = "+";
                    taxes_text.text += taxes_balance.ToString();
                    taxes_text.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
                }

                else
                {
                    taxes_text.text = taxes_balance.ToString();
                    taxes_text.color = new Color(0.2f, 0.7f, 0.2f, 1.0f);
                }

                last_frame_taxes = blackboard.GetValue<int>("taxes");
                StartCoroutine(FadeTextToZeroAlpha(fade_time, taxes_text));
            }
            else
                first_time = false;
        }


        last_frame_taxes = blackboard.GetValue<int>("taxes");
        last_frame_money = blackboard.GetValue<int>("money");
    }


    public IEnumerator FadeTextToZeroAlpha(float t, Text i)
    {
        i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
        while (i.color.a > 0.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a - (Time.unscaledDeltaTime / t));
            yield return null;
        }
    }
}


