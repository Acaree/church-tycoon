﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class OnClick_missionary : MonoBehaviour {

    GlobalBlackboard blackboard;
    public float happines_increase;

    void Start()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
    }

    public void Onclick()
    {
        int priest_number = blackboard.GetValue<int>("devouts_number");

        if (priest_number > 0)
        {
            blackboard.SetValue("devouts_number", priest_number - 1);
            blackboard.SetValue("votary_happines", blackboard.GetValue<float>("votary_happines") + happines_increase);
        }
    }
}
