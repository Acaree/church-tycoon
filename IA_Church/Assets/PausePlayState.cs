﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePlayState : MonoBehaviour {

    public GameObject go;

    public void PauseGame()
    {
        Time.timeScale = 0;
        go.SetActive(true);
        go.GetComponent<CalculateBillboard>().CalculateAllBillboard();
    }

    public void ContinueGame()
    {
        Time.timeScale = 1.0f;
        go.SetActive(false);
    }

    public void Restart()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("SampleScene");
        
    }
}
