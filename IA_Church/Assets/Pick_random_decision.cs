﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class Pick_random_decision : MonoBehaviour {

    // Use this for initialization
    public void SetRandomDialogueActive()
    {

        int rand = Random.Range(0, gameObject.transform.childCount);

        gameObject.transform.GetChild(rand).gameObject.SetActive(true);

    }
}
