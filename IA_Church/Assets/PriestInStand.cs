﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class PriestInStand : MonoBehaviour {

    public Light light;

    GlobalBlackboard blackboard;
    public Text total_priest;
    public Text total_stands;
    public Text text;
    public Slider slider;
    int value;
    // Use this for initialization
    void Start () {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");

    }
	
	// Update is called once per frame
	void Update () {

    

        if(gameObject.activeInHierarchy)
        {
            int priest_number = blackboard.GetValue<int>("priest_number");
            int stands_number = blackboard.GetValue<int>("stands_active");
            total_priest.text = "Priest: " + priest_number.ToString();
            total_stands.text = "Stands: " + stands_number.ToString();
            
            if(priest_number < stands_number)
            {
                transform.GetChild(transform.childCount-1).GetComponent<Slider>().maxValue = priest_number;
            }
            else
            {
                transform.GetChild(transform.childCount - 1).GetComponent<Slider>().maxValue = stands_number;
            }

            value = (int)slider.value;

            text.text = value.ToString();
           
        }
	}

    public void SetValue()
    {
        blackboard.SetValue("priest_to_stands", value);
    }


}
