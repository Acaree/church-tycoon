﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class PriestScript : MonoBehaviour
{

    Text text;
    GlobalBlackboard blackboard;
    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
    }

    // Update is called once per frame
    void Update()
    {
        text.color =  Color.black;
        int actual_priest = blackboard.GetValue<int>("priest_number");
        int max_number = blackboard.GetValue<int>("max_buy_number_priest");

        text.text = "Priest: " + actual_priest.ToString() + "/" + max_number.ToString();

        if (actual_priest == max_number)
        {
            text.color = Color.green;
            text.text = "Priest: " + actual_priest.ToString() + "/" + max_number.ToString();
        }
        else
            text.text = "Priest: " + actual_priest.ToString() + "/" + max_number.ToString();
    }
}