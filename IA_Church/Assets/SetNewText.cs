﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class SetNewText : MonoBehaviour {

    GlobalBlackboard blackboard;
    public Slider slider;
    float last_value = 0;

    private void Start()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
    }

    private void Update()
    {
        float value = blackboard.GetValue<float>("stand_bar_prob");

        if(last_value != value)
        {
            last_value = value;
            slider.value = value / 100;

        }

    }





}
