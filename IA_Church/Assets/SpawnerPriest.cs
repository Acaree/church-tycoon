﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class SpawnerPriest : MonoBehaviour {

    //Controllers Respawn
    public float time_to_respawn = 0.5f;
    public float time_to_repeat = 0.5f;
    public bool its_night = false;
    public bool invoke_used = false;
    public int priest_cost;

    public GameObject go;
   
    GameObject button_priest;
    int respawned = 0;

    Light light;
    GlobalBlackboard blackboard;
    int max_number;
    // Use this for initialization
    void Start()
    {
        light = GameObject.Find("Directional Light").GetComponent<Light>();
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
    }

    // Update is called once per frame
    void Update()
    {

        if (light.tag == "Day" && !invoke_used)
        {
            InvokeRepeating("Respawn", time_to_respawn, time_to_repeat);
            invoke_used = true;
        }
        else if (light.tag == "Night" && invoke_used)
        {
            CancelInvoke();
            invoke_used = false;
            its_night = true;
            respawned = 0;
        }
    }


    void Respawn()
    {
        int priest_to_stand = blackboard.GetValue<int>("priest_to_stands");
        max_number = blackboard.GetValue<int>("priest_number");

        if (respawned < max_number)
        {
            GameObject go_respawned = Instantiate(go, transform.position, transform.rotation);

            if(priest_to_stand != 0)
            {
                
                go_respawned.GetComponent<Blackboard>().SetValue("go_to_stand",true);
                blackboard.SetValue("priest_to_stands", priest_to_stand - 1);
            }

            respawned++;
        }

    }

    public void SpawnPriest()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
        max_number = blackboard.GetValue<int>("priest_number");

        if (max_number != blackboard.GetValue<int>("max_buy_number_priest"))
        {
            blackboard.SetValue("priest_number", max_number + 1);
            blackboard.SetValue("money", blackboard.GetValue<int>("money") - priest_cost);
        }
        else
        {
            button_priest = GameObject.Find("AddPriest_Button");
            button_priest.GetComponent<Button>().interactable = false;
            
            button_priest.GetComponentInChildren<Text>().text = "Max number adquired";
        }
    }



}
