﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class StandBarUI : MonoBehaviour {


    GlobalBlackboard blackboard;

    public int stand_to_affect;

    float last_prob1 = 0.0f;
    float last_prob2 = 0.0f;
    float last_prob3 = 0.0f;

    // Use this for initialization
    void Start () {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
        last_prob1 = blackboard.GetValue<float>("Home1_become_devout");
        last_prob2 = blackboard.GetValue<float>("Home2_become_devout");
        last_prob3 = blackboard.GetValue<float>("Home3_become_devout");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeProbabilityStand()
    {
        float value = GetComponent<Slider>().value;

        switch (stand_to_affect)
        {
            case 0:
                blackboard.SetValue("Home1_become_devout", last_prob1 * value);
                blackboard.SetValue("Home2_become_devout", last_prob2 * value);
                blackboard.SetValue("Home3_become_devout", last_prob3 * value);
                break;
            case 1:
                blackboard.SetValue("Home1_become_devout", last_prob1 * value);
                break;
            case 2:
                blackboard.SetValue("Home2_become_devout", last_prob2 * value);
                break;
            case 3:
                blackboard.SetValue("Home3_become_devout", last_prob3 * value);
                break;
            case 12:
                blackboard.SetValue("Home1_become_devout", last_prob1 * value);
                blackboard.SetValue("Home2_become_devout", last_prob2 * value);
                break;
            case 13:
                blackboard.SetValue("Home1_become_devout", last_prob1 * value);
                blackboard.SetValue("Home3_become_devout", last_prob3 * value);
                break;
            case 23:
                blackboard.SetValue("Home2_become_devout", last_prob2 * value);
                blackboard.SetValue("Home3_become_devout", last_prob3 * value);
                break;
        }

      

    }

}
