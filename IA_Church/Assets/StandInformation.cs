﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;
using UnityEngine.EventSystems;

public class StandInformation : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{

    public GameObject image;
    public int stand_type; 

    //TODO WHEN WE use the global blackboard for conversion rate , finish this.

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        GlobalBlackboard blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
        image.SetActive(true);

        switch(stand_type)
        {
            case 0:

                image.GetComponentInChildren<Text>().text = "Probability to convert: " + blackboard.GetValue<float>("Home1_become_devout").ToString() + "\n" + "Probability to visit another stand: " + blackboard.GetValue<float>("Home1_visit_stand").ToString();

                break;
            case 1:
                image.GetComponentInChildren<Text>().text = "Probability to convert: " + blackboard.GetValue<float>("Home2_become_devout").ToString() + "\n" + "Probability to visit another stand: " + blackboard.GetValue<float>("Home2_visit_stand").ToString();
                break;
            case 2:
                image.GetComponentInChildren<Text>().text = "Probability to convert: " + blackboard.GetValue<float>("Home3_become_devout").ToString() + "\n" + "Probability to visit another stand: " + blackboard.GetValue<float>("Home3_visit_stand").ToString();
                break;
        }
        image.GetComponentInChildren<Text>().text +="\n" +"Taxes +300";
    }

   

    public void OnPointerDown(PointerEventData pointerEventData)
    {
        image.SetActive(false);
    }
}
