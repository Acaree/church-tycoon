﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class ToggleButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        int money = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB").GetValue<int>("money");

        if(money <= 0)
        {
            GetComponent<Button>().interactable = false;
        }
        else
            GetComponent<Button>().interactable = true;
    }
}
