﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ToggleOptions : MonoBehaviour, IPointerClickHandler
{

    public GameObject[] game_objects;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerClick(PointerEventData eventData) 
    {
        foreach (GameObject go in game_objects)
            go.SetActive(!go.activeSelf);
    }

}
