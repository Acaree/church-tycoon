﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class TotalMoney : MonoBehaviour {


    GlobalBlackboard blackboard;
    public GameObject go;

	// Use this for initialization
	void Start () {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");

    }
	
	// Update is called once per frame
	void Update () {
        go.GetComponent<CalculateBillboard>().Taxes();
        GetComponentInChildren<Text>().text = "Money: " + blackboard.GetValue<int>("money").ToString() + "\n" + "Taxes: " +blackboard.GetValue<int>("taxes").ToString();
	}
}
