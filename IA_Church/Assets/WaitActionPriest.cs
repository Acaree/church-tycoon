using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine.UI;
using UnityEngine;

namespace NodeCanvas.Tasks.Actions{

	public class WaitActionPriest : ActionTask{

        AudioSource source;
        
        float messes_time = 0.0f;
        protected override string OnInit()
        {
            source = agent.GetComponent<AudioSource>();
            return null;
        }

        //This is called once each time the task is enabled.
        //Call EndAction() to mark the action as finished, either in success or failure.
        //EndAction can be called from anywhere.
        protected override void OnExecute()
        {
            source.Play();

            agent.GetComponentInChildren<Image>(true).enabled = true;

        }

        //Called once per frame while the action is active.
        protected override void OnUpdate()
        {
            if (messes_time >= 7.0f)
            {
                agent.GetComponentInChildren<Image>(true).enabled = false;
                source.Stop();
                EndAction(true);
            }
            else
            {
                messes_time += Time.deltaTime;
            }


        }
    }
}