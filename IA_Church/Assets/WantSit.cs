using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace NodeCanvas.Tasks.Conditions{

	public class WantSit : ConditionTask{

        public BBParameter<bool> search_destination;
		//Use for initialization. This is called only once in the lifetime of the task.
		//Return null if init was successfull. Return an error string otherwise
		protected override string OnInit(){
			return null;
		}

		//Called once per frame while the condition is active.
		//Return whether the condition is success or failure.
		protected override bool OnCheck(){


            if (NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB").GetValue<bool>("do_masses"))
            {
                search_destination.value = true;
                return true;
            }
			return false;
		}
	}
}