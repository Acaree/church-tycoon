﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;


public class onClickChanceButtons : MonoBehaviour {

    GlobalBlackboard blackboard;
    public int money_lose;
    public float bar_increase1;
    public float bar_increase2;
    public float happines_lose;
    

    void Start()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");

    }

    public void Onclick1()
    {
        blackboard.SetValue("money", blackboard.GetValue<int>("money") - money_lose);
        blackboard.SetValue("stand_bar_prob", blackboard.GetValue<float>("stand_bar_prob") + bar_increase1);
    }

    public void Onclick2()
    {
        blackboard.SetValue("votary_happines", blackboard.GetValue<float>("votary_happines") - happines_lose);
        blackboard.SetValue("stand_bar_prob", blackboard.GetValue<float>("stand_bar_prob") + bar_increase2);
    }


}
