﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;


public class onclick_charity : MonoBehaviour {

    GlobalBlackboard blackboard;
    public int charity_money;
    public float happines_increase;

    void Start()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");

    }

    public void Onclick()
    {
        blackboard.SetValue("money", blackboard.GetValue<int>("money") - charity_money);
        blackboard.SetValue("votary_happines", blackboard.GetValue<float>("votary_happines") + happines_increase);
    }
}
