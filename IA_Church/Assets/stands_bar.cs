﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.UI;

public class stands_bar : MonoBehaviour {

    private float time_counter = 0;
    public float time_to_downgrade_happines;
    public Slider UI_slider;
    GlobalBlackboard blackboard;

    public Light light;

    // Use this for initialization
    void Start()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
        UI_slider.value = blackboard.GetValue<float>("stand_bar_prob") / 100;
    }

    // Update is called once per frame
    void Update()
    {

        UI_slider.value = blackboard.GetValue<float>("stand_bar_prob") / 100;

        if (light.tag == "Day")
        {
            time_counter += Time.deltaTime;

            if (time_counter >= time_to_downgrade_happines)
            {
                time_counter = 0.0f;
                UI_slider.value -= 0.05f;
                blackboard.SetValue("stand_bar_prob", UI_slider.value * 100);

            }
        }

    }
}
