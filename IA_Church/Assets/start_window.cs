﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class start_window : MonoBehaviour {

    private void Start()
    {
        Time.timeScale = 0;
    }

    public void Onclick()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1.0f;
    }
}
