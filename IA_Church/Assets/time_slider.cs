﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class time_slider : MonoBehaviour {

    public GameObject time_text;

	// Use this for initialization
	void Start () {

        float time_multiplier = GetComponent<Scrollbar>().value * 2;
        time_text.GetComponentInChildren<Text>().text = time_multiplier.ToString();
    }

    void Update()
    {

        GetComponent<Scrollbar>().value = Time.timeScale/2;
        time_text.GetComponentInChildren<Text>().text = Time.timeScale.ToString();
    }

    public void OnSlide()
    {
        float time_multiplier = GetComponent<Scrollbar>().value * 2;
        Time.timeScale = time_multiplier;
        time_text.GetComponentInChildren<Text>().text = time_multiplier.ToString();

    }
}
