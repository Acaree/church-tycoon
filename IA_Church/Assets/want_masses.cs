using NodeCanvas.Framework;
using ParadoxNotion.Design;
using UnityEngine;

namespace NodeCanvas.Tasks.Conditions{


	public class want_masses : ConditionTask{

        public BBParameter<GameObject> podium;
        float masses_probability = 0.5f;

        bool last_time_messes = false;
		//Use for initialization. This is called only once in the lifetime of the task.
		//Return null if init was successfull. Return an error string otherwise
		protected override string OnInit(){
			return null;
		}

		//Called once per frame while the condition is active.
		//Return whether the condition is success or failure.
		protected override bool OnCheck(){

            if (!NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB").GetValue<bool>("do_masses"))
            {

                if (podium.value.GetComponent<AvaliablePoint>().avaliable)
                {
                    if (!last_time_messes)
                    {
                        last_time_messes = true;
                        agent.GetComponent<Blackboard>().SetValue("number_of_masses", agent.GetComponent<Blackboard>().GetValue<int>("number_of_masses") + 1);

                        podium.value.GetComponent<AvaliablePoint>().avaliable = false;

                        NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB").SetValue("do_masses", true);
                        masses_probability = 0.5f;
                        return true;
                    }
                    else
                        last_time_messes = false;

                }

            }
            return false;
           

		}
	}
}