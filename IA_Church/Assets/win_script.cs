﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NodeCanvas.Framework;

public class win_script : MonoBehaviour {

    public GameObject win_screen;
    public Text days_text;
    GlobalBlackboard blackboard;
    // Use this for initialization
    void Start()
    {
        blackboard = NodeCanvas.Framework.GlobalBlackboard.Find("Church_BB");
    }

    // Update is called once per frame
    void Update () {
		
        if(blackboard.GetValue<int>("priest_number") >= blackboard.GetValue<int>("max_buy_number_priest") && blackboard.GetValue<int>("devouts_number") >= blackboard.GetValue<int>("max_number_devouts"))
        {
            win_screen.gameObject.SetActive(true);
            days_text.text = blackboard.GetValue<int>("day_count").ToString();
            Time.timeScale = 0;
        }

	}
}
