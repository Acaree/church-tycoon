﻿# Church Tycoon

---

## About the game

This game is about manage a church. The player can buy more stands to get more chance to transform citizens into votaries and can also contract
new priests. Player earn money everytime a priest give a mass.

To know more about our game, visit our [wiki](https://bitbucket.org/Acaree/church-tycoon/wiki/Home)

### Controls

-W/A/S/D or Move mouse to screen border: Move camera

- If "Buy" button is pressed the three possible stans to buy will appear. Click on the one you want to buy and after that click on one green square (they're
all in the garden of the church) to build.

- "Hire another priest" Button will make appear another priest.

- At the end of the day, if money is above 0 you'll lose and a "restart" button will appear to reset the game. If not, another day will begin when "continue" button is pressed.

---

## Authors

Alex Campamar

Alfonso Sanchez-Cortes

**Link to the bitbucket repository: https://bitbucket.org/Acaree/church-tycoon/src/master/** 

---

## License

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.